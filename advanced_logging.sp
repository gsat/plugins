#pragma semicolon 1
#include <sourcemod>
#include <uuid>
#include <sdktools>
#include <geoip>

new String:varUniqueUserId[10] = "sv_logecho";
new String:sClientConvar[MAXPLAYERS+1][37];

public Plugin myinfo =
{
    name = "Advanced logging",
    author = "ExoIT",
    description = "Advanced logging",
    version = SOURCEMOD_VERSION,
    url = ""
};

public OnPluginStart()
{
    /*
     * Fix standard logging messages
     */

    // original example "Imba<2209><STEAM_ID_PENDING><>" connected, address "192.168.0.2:27005"
    HookEvent("player_connect", PlayerConnect, EventHookMode_Pre);

    // original example "Imba<2209><STEAM_ID_PENDING><>" entered the game
    HookEvent("player_activate", PlayerActivate, EventHookMode_Pre);

    // original example "Imba<2209><STEAM_ID_PENDING><Unassigned>" disconnected (reason "Disconnect by user.")
    HookEvent("player_disconnect", PlayerDisconnect, EventHookMode_Pre);

    // original example "Imba<2209><UUID><Unassigned>" attacks "Ted<2213><BOT><CT>"
    HookEvent("player_hurt", PlayerHurt, EventHookMode_Pre);

    // original example "Imba<2209><STEAM_ID_PENDING><TERRORIST>" killed "Ted<2213><BOT><CT>" with "mp5navy"
    HookEvent("player_death", PlayerDeath, EventHookMode_Pre);

    // original example "Imba<2209><STEAM_ID_PENDING><Unassigned>" joined team "TERRORIST"
    HookEvent("player_team", PlayerTeam, EventHookMode_Pre);

    // original example "Imba<2209><STEAM_ID_PENDING><TERRORIST>" say "ff"
    AddCommandListener(PlayerSay, "say");

    // original example "Imba<2209><STEAM_ID_PENDING><TERRORIST>" say_team "ff"
    AddCommandListener(PlayerSayTeam, "say_team");

    // advanced status
    RegConsoleCmd("adv_status", AdvancedStatus);
}

public Action:PlayerConnect(Handle:event, const String:name[], bool:dontBroadcast) { return Plugin_Handled; }
public Action:PlayerActivate(Handle:event, const String:name[], bool:dontBroadcast) { return Plugin_Handled; }

public Action:PlayerDisconnect(Handle:event, const String:name[], bool:dontBroadcast)
{
    new userid = GetEventInt(event, "userid");
    new client = GetClientOfUserId(userid);

    if (client && !IsFakeClient(client))
    {
        decl String:reason[128];
        GetEventString(event, "reason", reason, sizeof(reason));

        decl String:sBuffer[512];
        Format(sBuffer, sizeof(sBuffer), "\"%N<%i><%s><Unassigned>\" disconnected (reason \"%s\")", client, userid, sClientConvar[client], reason);
        LogToGame(sBuffer);

        strcopy(sClientConvar[client], 0, "");
    }

    return Plugin_Handled;
}

public Action:PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast)
{
    new srcUserId = GetEventInt(event, "attacker"); // who kill
    new dstUserId = GetEventInt(event, "userid"); // who dead

    new srcClient = GetClientOfUserId(srcUserId);
    new dstClient = GetClientOfUserId(dstUserId);

    new damage = GetEventInt(event, "dmg_health");
    new damageArmor = GetEventInt(event, "dmg_armor");
    new hitgroup = GetEventInt(event, "hitgroup");

    new hitGeneric = 0, hitHead = 0, hitChest = 0, hitStomach = 0, hitLeftHand = 0, hitRightHand = 0, hitLeftLeg = 0, hitRightLeg = 0, hitGear = 0;
    new hitGenericArmor = 0, hitHeadArmor = 0, hitChestArmor = 0, hitStomachArmor = 0, hitLeftHandArmor = 0, hitRightHandArmor = 0, hitLeftLegArmor = 0, hitRightLegArmor = 0, hitGearArmor = 0;

    switch(hitgroup)
    {
        case 0: // Generic
        {
            hitGeneric = damage;
            hitGenericArmor = damageArmor;
        }
        case 1: // Head
        {
            hitHead = damage;
            hitHeadArmor = damageArmor;
        }
        case 2: // Chest
        {
            hitChest = damage;
            hitChestArmor = damageArmor;
        }
        case 3: // Stomach
        {
            hitStomach = damage;
            hitStomachArmor = damageArmor;
        }
        case 4: // Left hand
        {
            hitLeftHand = damage;
            hitLeftHandArmor = damageArmor;
        }
        case 5: // Right hand
        {
            hitRightHand = damage;
            hitRightHandArmor = damageArmor;
        }
        case 6: // Left leg
        {
            hitLeftLeg = damage;
            hitLeftLegArmor = damageArmor;
        }
        case 7: // Right leg
        {
            hitRightLeg = damage;
            hitRightLegArmor = damageArmor;
        }
        case 10: // Gear
        {
            hitGear = damage;
            hitGearArmor = damageArmor;
        }
    }

    decl String:sBuffer[512], String:sDamage[128], String:sArmorDamage[128];

    Format(sDamage, sizeof(sDamage), "<%i><%i><%i><%i><%i|%i><%i|%i><%i>", hitGeneric, hitHead, hitChest, hitStomach, hitLeftHand, hitRightHand, hitLeftLeg, hitRightLeg, hitGear);
    Format(sArmorDamage, sizeof(sArmorDamage), "<%i><%i><%i><%i><%i|%i><%i|%i><%i>", hitGenericArmor, hitHeadArmor, hitChestArmor, hitStomachArmor, hitLeftHandArmor, hitRightHandArmor, hitLeftLegArmor, hitRightLegArmor, hitGearArmor);

    Format(sBuffer, sizeof(sBuffer), "\"%N<%i><%s><Unknown>\" attacks \"%N<%i><%s><Unknown>\" damage %s armor %s", srcClient, srcUserId, sClientConvar[srcClient], dstClient, dstUserId, sClientConvar[dstClient], sDamage, sArmorDamage);
    LogToGame(sBuffer);

    return Plugin_Continue;
}

public Action:PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
    SetEventBroadcast(event, true);

    new srcUserId = GetEventInt(event, "attacker"); // who kill
    new dstUserId = GetEventInt(event, "userid"); // who dead

    new srcClient = GetClientOfUserId(srcUserId);
    new dstClient = GetClientOfUserId(dstUserId);

    new String:byWeapon[100];
    GetEventString(event, "weapon", byWeapon, sizeof(byWeapon));

    new String:sBuffer[512];
    Format(sBuffer, sizeof(sBuffer), "\"%N<%i><%s><Unknown>\" killed \"%N<%i><%s><Unknown>\" with \"%s\"", srcClient, srcUserId, sClientConvar[srcClient], dstClient, dstUserId, sClientConvar[dstClient], byWeapon);
    LogToGame(sBuffer);

    Event customEvent = CreateEvent("player_death");
    SetEventBroadcast(customEvent, true);

    customEvent.SetInt("attacker", srcUserId);
    customEvent.SetInt("userid", dstUserId);

    customEvent.SetString("weapon", byWeapon);
    customEvent.SetBool("headshot", GetEventBool(event, "headshot"));
    customEvent.SetInt("dominated", GetEventInt(event, "dominated"));
    customEvent.SetInt("revenge", GetEventInt(event, "revenge"));
    
    for (int i = 1; i < MaxClients; i++)
    {
        if (IsClientInGame(i) && !IsFakeClient(i))
            customEvent.FireToClient(i);
    }

    return Plugin_Handled;
}
public Action:PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast) { return Plugin_Handled; }

public Action:PlayerSay(client, const String:command[], argc)
{
    new String:message[512];
    new String:sMessageBuffer[512];

    GetCmdArgString(message, sizeof(message));
    StripQuotes(message);

    EmitSoundToAll("ui/buttonrollover.wav");
    Format(sMessageBuffer, sizeof(sMessageBuffer), "[\x04Player\x01] \x03%N \x01: %s", client, message);
    PrintToChatAll(sMessageBuffer);

    new String:sBuffer[512];
    Format(sBuffer, sizeof(sBuffer), "\"%N<%i><%s><Unknown>\" say \"%s\"", client, GetClientUserId(client), sClientConvar[client], message);
    LogToGame(sBuffer);

    return Plugin_Handled;
}

public Action:PlayerSayTeam(client, const String:command[], argc)
{
    PrintToChat(client, "Sorry, say tem disabled on this server");
    return Plugin_Handled;
}

public Action:AdvancedStatus(client, args)
{
    decl String:sBuffer[512], String:ipAddress[16], String:ipAddressWithPort[24], String:country[64];

    for (int i = 1; i < MaxClients; i++)
    {
        if (IsClientInGame(i) && !IsFakeClient(i))
        {
            GetClientIP(i, ipAddress, sizeof(ipAddress), true);
            GetClientIP(i, ipAddressWithPort, sizeof(ipAddressWithPort), false);

            GeoipCountry(ipAddress, country, sizeof(country));

            Format(sBuffer, sizeof(sBuffer), "\"%N<%i><%s>\" <%s> \"%s\"", i, GetClientUserId(i), sClientConvar[i], country, ipAddressWithPort);
            PrintToConsole(client, sBuffer);
        }
    }

    return Plugin_Handled;
}

public OnClientPutInServer(client)
{
    decl String:sBuffer[512], String:ipAddress[16], String:ipAddressWithPort[24], String:country[64];
    GetClientIP(client, ipAddress, sizeof(ipAddress), false);

    GetClientIP(client, ipAddress, sizeof(ipAddress), true);
    GetClientIP(client, ipAddressWithPort, sizeof(ipAddressWithPort), false);

    GeoipCountry(ipAddress, country, sizeof(country));

    Format(sBuffer, sizeof(sBuffer), "\"%N<%i><><Unassigned>\" connected, address \"%s\" from <%s>", client, GetClientUserId(client), ipAddressWithPort, country);
    LogToGame(sBuffer);

    if (IsClientInGame(client) && !IsFakeClient(client))
        QueryClientConVar(client, varUniqueUserId, ConVarQueryFinished:ConVarFinishedCallBack, client);
}

public ConVarFinishedCallBack(QueryCookie:cookie, client, ConVarQueryResult:result, const String:cvarName[], const String:cvarValue[])
{
    if (result == ConVarQuery_Okay)
    {
        strcopy(sClientConvar[client], sizeof(sClientConvar[]), cvarValue);
        CreateTimer(0.01, TimerWelcome, client, TIMER_FLAG_NO_MAPCHANGE);
    }
    else if (IsClientConnected(client))
    {
        KickClient(client, "You are not authorized");
    }
}

public Action:TimerWelcome(Handle:timer, any:client)
{
    if (client && IsClientInGame(client))
    {
        new Handle:hMessage = CreateKeyValues("data");
        decl String:sBuffer[512];

        Format(sBuffer, sizeof(sBuffer), "");
        KvSetString(hMessage, "title", sBuffer);
        KvSetString(hMessage, "type", "2");

        Format(sBuffer, sizeof(sBuffer), "http://wthell.org.ua/motd.html");
        KvSetString(hMessage, "msg", sBuffer);

        if (strlen(sClientConvar[client]) < 36)
        {
            char randomString[37];
            GenerateUUID(randomString, sizeof(randomString));

            Format(sBuffer, sizeof(sBuffer), "%s \"%s\";joingame", varUniqueUserId, randomString);
            KvSetString(hMessage, "cmd", sBuffer);

            sClientConvar[client] = randomString;
        }
        else
        {
            KvSetString(hMessage, "cmd", "joingame");
        }

        ShowVGUIPanel(client, "info", hMessage, true);
        CloseHandle(hMessage);

        //decl String:ipAddress[24];
        //GetClientIP(client, ipAddress, sizeof(ipAddress), false);
        //Format(sBuffer, sizeof(sBuffer), "\"%N<%i><%s><Unassigned>\" connected, address \"%s\"", client, GetClientUserId(client), sClientConvar[client], ipAddress);

        Format(sBuffer, sizeof(sBuffer), "\"%N<%i><%s><Unassigned>\" entered the game", client, GetClientUserId(client), sClientConvar[client]);
        LogToGame(sBuffer);
    }

    return Plugin_Stop;
}
