## Advanced Logging (Replacing the standard server logging system)

| Dependency            | Version  | Download link                                                                              |
| --------------------- | -------- | ------------------------------------------------------------------------------------------ |
| Counter-Strike Source | 1.0.0.34 | [Linux or Windows server](http://css.setti.info/forum/topic/1375-srcds-v34/)               |
| SourceMod             | 1.9+     | [SourceMod for v1.0.0.34 linux only](https://bitbucket.org/account/user/_4/projects/SM_34) |